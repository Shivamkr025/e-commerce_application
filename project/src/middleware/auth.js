require('dotenv').config();
const jwt = require('jsonwebtoken');

console.log(process.env.secretKey);
const createToken = (email) => {
    return jwt.sign({ email }, process.env.secretKey, { expiresIn: '2h' });
};

const verifyToken = async (req, res, next) => {
    try {
        if (req.headers.cookie) {
            const token = req.headers.cookie.split("=")[1];
            console.log(token);

            if (!token) {
                return res.status(400).json({ error: "Invalid token format" });
            }

            const check = jwt.verify(token, process.env.secretKey);
            req.check = check;
            next();

        } else {
            return res.status(400).json({ message: "Please log in to your account." });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: "Internal issue. Please check the verifyToken function." });
    }
};

module.exports = { createToken, verifyToken };
