const Admin = require("../../controllers/admin/signupMember")
const {verifyToken} = require("../../middleware/auth")
const express = require('express')

const router = express.Router()

router.post('/Admin-signup' ,Admin.AdminSignup);

router.post('/Admin-login' , Admin.AdminLogin);

router.post('/Admin-update' , verifyToken , Admin.updateAdmin)

router.post('/Admin-remove' , verifyToken , Admin.AdminRemove)

router.get('/All-Admin' ,verifyToken , Admin.viweAdmin)

module.exports = router