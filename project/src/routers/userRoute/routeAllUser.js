const User = require('../../controllers/user/userControllers')
const express = require('express')

const router = express.Router()

router.get('/view-user' , User.viewAllUser)

router.post('/User-Signup' , User.userSignup)

router.post('/User-Login' , User.userLogin)

router.post('/User-update' , User.updateUser)

router.post('/delete-user' , User.deleteUser)


module.exports = router