const signup = require('../../models/allCollection').signup;
const { createToken } = require('../../middleware/auth');
const bcrypt = require('bcrypt');
const saltAround = 10;


const AdminSignup = async (req, res) => {
    const { email, password } = req.body
    try {
        const AdminData = await signup.findOne({ email })
        if (AdminData) {
            return res.status(400).json({ error: "Admin is already signup..." })
        }

        const passwordHash = await bcrypt.hash(password, saltAround)
        const submit = new signup({ ...req.body, password: passwordHash })
        await submit.save()
        res.status(200).json({ message: "successfully signup Admin..." })
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: "something went wrong in signup function" })
    }

}

const AdminLogin = async (req, res) => {
    const { email, password } = req.body
    try {
        const AdminData = await signup.findOne({ email })
        if (!AdminData) {
            return res.status(400).json({ error: "please signup your account ..." })
        }

        const comparePassword = await bcrypt.compare(password, AdminData.password)

        if (comparePassword) {

            const token = createToken(email)
            res.cookie('token', token)
            res.status(200).json({ message: "successfully login Admin..."})
        }else{
            return res.status(400).json({error:"password is not matching..."})
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({ error: "something went wrong in Admin login function" })
    }
}

const updateAdmin = async(req , res) =>{
    
    const {email,userId , fullName , role , password} = req.body
    try {
        const adminData = await signup.findOne({email})
        if(!adminData){
            return res.status(400).json({error:"please signup your account ..."})
        }
        
        const checkRole = await signup.findOne({role})
        if(!checkRole){
            return res.status(400).json({error:"please mention your role otherwise you can't update...."})
        }

        const passwordHash = await bcrypt.hash(password , saltAround)

        const updateDetails = await signup.findOneAndUpdate({email}, {$set:{userId , fullName , role , password:passwordHash }} , {new:true});
        res.status(200).json({message:"details update successfully...", updateDetails})

    } catch (error) {
        console.log(error);
        res.status(500).json({error:"something is wrong in update function"})
    }
}

const AdminRemove = async(req , res)=>{
    const {email} = req.body
    try {
        const adminData = await signup.findOne({email})
        if (!adminData){
            return res.status(400).json({error:"This email is not available in database"})
        }

        await signup.findOneAndDelete({email})
        res.status(200).json({message:"successfully delete the account"})
    } catch (error) {
        console.log(error);
        res.status(500).json({error:"something is worng in remove function"})
    }
}

const viweAdmin = async(req , res)=>{
    try {
        const AllAdmin = await signup.find({} , {_id:0 ,password:0 , __v:0})
        res.status(200).json(AllAdmin)
    } catch (error) {
        console.log(error);
        res.status(500).json({error:"something is wrong in viewAdmin"})
    }
}




module.exports = { AdminSignup, AdminLogin , updateAdmin , AdminRemove , viweAdmin};
