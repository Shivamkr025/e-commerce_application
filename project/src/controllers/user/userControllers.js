const {signup} = require('../../models/allCollection')
const bcrypt = require('bcrypt')
const saltAround = 10

const viewAllUser = async(req , res)=>{
    try {
        const findData = await signup.find({},{_id:0 , password:0 , __v:0});
        res.status(200).json({findData})
    } catch (error) {
        
    }
}


const userSignup = async(req , res)=>{
    const {email , password} = req.body
    try {
        const userData = await signup.findOne({email})
        if (userData){
            return res.status(400).json({error:"user already create account "})
        }
        
        const passwordHash = await bcrypt.hash(password , saltAround)
        const submit = new signup({...req.body , password:passwordHash})
        await submit.save()
        res.status(201).json({message:"create account successfully"})
    } catch (error) {
        console.log(error);
        res.status(500).json({error:"something went wrong in user signup function "})
    }
}

const userLogin = async(req , res) =>{
        const {email , password} = req.body
        try {
            const userData = await signup.findOne({email})
            if(!userData){
                return res.status(400).json({error:"please signup your account ..."})
            }

            if(userData.role !== "user"){
                return res.status(400).json({error:"This email is not match with user role"})
            }
        
            const passwordCompare = await bcrypt.compare(password , userData.password)
            if (passwordCompare){
                return res.status(200).json({message:"user login successfully "})
            }else{
                return res.status(400).json({error:"please check your password"})
            }
        } catch (error) {
            console.log(error);
            res.status(500).json({error:"something went wrong in login function"})
        }
}

const updateUser = async(req , res)=>{
    const {fullName , email , password} = req.body
    try {
        const userData = await signup.findOne({email})
        if (!userData){
            return res.status(400).json({error: "please create your account "})
        }
        if (userData.userId){
            const updateData = await signup.findOneAndUpdate({email} , {$set:{userId ,fullName, password}}, {new:true})
            return res.status(200).json({message:"successfully update ", updateData})
        }else{
            return res.status(400).json({error:"data is not update"})
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({error:"something is wrong in update user function"})
    }
}

const deleteUser = async(req , res)=>{
    const {email} = req.body
    try {
        const checkData = await signup.findOne({email})
        if (!checkData){
            return res.status(400).json({error:"This email is not exist in db..."})
        }

        await signup.findOneAndDelete({email})
        res.status(200).json({email:"successfully deleted "})
    } catch (error) {
        console.log(error);
        res.status(500).json({error:"something is wrong in delete function "})
    }
}

module.exports = { viewAllUser ,userSignup , userLogin , updateUser ,  deleteUser}