
const mongoose = require('mongoose')

const memberSchema = new mongoose.Schema({
    fullName: String,
    role: String,
    email: String,
    password: String
})

const productSchema = new mongoose.Schema({
    productId: String,
    productName: String,
    price: String,
    quentity: String

})

const orderSchema = new mongoose.Schema({
    orderId: String,
    productId: String,
    email: String,
    product: String,
    total: Number

})

const signup = mongoose.model('signup', memberSchema)
const product = mongoose.model('product', productSchema)
const order = mongoose.model('order' , orderSchema)

module.exports = { signup , product , order }



