const express = require('express')
const mongoose = require('mongoose')
const file = require('./src/routers/adminRoute/routeAllAdmin')
const userFile = require('./src/routers/userRoute/routeAllUser')

const PORT = 6500
const app = express()
app.use(express.json())
app.use('/', file)
app.use('/' , userFile)

mongoose.connect("mongodb://localhost:27017/e-commerces")
    .then(() => {
        console.log("connecting successfully...");
    }).catch((error) => {
        console.log(error);
    })

app.listen(PORT , ()=>{
    console.log(`listening to the server ${PORT}`);
})